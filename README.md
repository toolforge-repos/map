= Tool-Map - Spread of COVID-19 =

{image uri=https://upload.wikimedia.org/wikipedia/commons/thumb/c/cd/Wikidata_stamp.png/288px-Wikidata_stamp.png, width=200px, height=200px, alt=Powered by Wikidata, href=https://www.wikidata.org/wiki/Wikidata:Main_Page}
{image uri=https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/Toolforge_logo_with_text.svg/225px-Toolforge_logo_with_text.svg.png, width=200px, height=200px, alt=Logo toolforge, href=https://tools.wmflabs.org/}

**Tool-map** is an interactiv SVG map showing number of infected, deaths, and tested connected to the COVID-19 disease. Updated once a day with numbers from Wikidata.

* <https://tools.wmflabs.org/map/> {icon cog spin}

== To-do list ==
- [ ] Phone and tablet support.
- [ ] Graphs for each country.
- [ ] Support for multiple languages.
- [X] Support access to raw data. (https://tools.wmflabs.org/map/raw.json)
